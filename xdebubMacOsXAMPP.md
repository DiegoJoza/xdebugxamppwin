﻿# Xdebug php 5.6 Windows con XAMPP
Descargar la extensión en VSC
```sh
PHP DEBUG 
PHP intellisente
```
Instalar Xdebug 
- Descargar desde [https://xdebug.org/download.php](https://xdebug.org/download.php) la versión recomendada [PHP 5.6 VC11 TS (32 bits)](https://xdebug.org/files/php_xdebug-2.5.5-5.6-vc11.dll)
- Al tener el archivo descargado lo copia a C:\xampp\php\ext
- Configurar el php.ini con lo siguiente: 
```sh
[XDebug]
zend_extension = "C:\xampp\php\ext\php_xdebug-2.5.5-5.6-vc11.dll"
xdebug.profiler_enable = 1
xdebug.remote_enable = 1
xdebug.remote_autostart = 1
xdebug.remote_handler = "dbgp"
xdebug.remote_host = "127.0.0.1"
xdebug.remote_port=9000
xdebug.trace_output_dir = "C:\xampp\tmp"
```
# configuracion VSC
- File- Preferences-Settings 
- Editar el archivo Settings.json con: 
```sh
{
    "editor.dragAndDrop": true,
    "phpserver.browser": "chrome",
    "php.validate.executablePath": "C:\\xampp\\php\\php.exe"
}
```

# Reinicar el VSC
- Debug 
- Add configuración y seleccionar PHP 


